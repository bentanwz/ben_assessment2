var express = require("express");
var bodyParser = require("body-parser");
var mysql =require("mysql");
var q = require("q");

var app = express ();

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

var pool = mysql.createPool({
    host: "ec2-54-169-245-152.ap-southeast-1.compute.amazonaws.com",
    port: 3306,
    user: "root",
    password: "password@123",
    database: "books",
    connectionLimit: 4
});

const findAllBooks = "SELECT id, title, cover_thumbnail, author_firstname, author_lastname FROM books LIMIT ? OFFSET ?";
const findOneBook = "SELECT * FROM books WHERE id = ?";
const saveOneBook = "UPDATE books SET title = ?, author_firstname = ?, author_lastname = ? WHERE id = ?";
const searchBooksByCriteria = "SELECT * FROM books WHERE (title LIKE ?) && (author_firstname LIKE ? || author_lastname LIKE ?)";
const searchBookByTitle = "SELECT * FROM books WHERE title LIKE ?";
const searchBookByName = "SELECT * FROM books WHERE author_firstname LIKE ? || author_lastname LIKE ?";

var makeQuery = function (sql, pool) {
    return function (args) {
        var defer = q.defer();
        pool.getConnection(function (err, conn) {
            if (err) {
                defer.reject(err);
                return;
            }
            conn.query(sql, args || [], function (err, results) {
                conn.release();
                if (err) {
                    defer.reject(err);
                    return;
                }
                defer.resolve(results);
            });
        });
        return defer.promise;
    }
};


var findAll = makeQuery(findAllBooks, pool);
var findOne = makeQuery(findOneBook, pool);
var saveOne = makeQuery(saveOneBook, pool);
var searchBooks = makeQuery(searchBooksByCriteria, pool);
var titleSearch = makeQuery(searchBookByTitle, pool);
var nameSearch = makeQuery(searchBookByName, pool);

//get "/" => redirect('views/index.html');

app.get("/api/books", function (req,res) {
    console.log(req.query.limit);
    console.log(req.query.offset);
    var limit = parseInt(req.query.limit) || 50;
    var offset = parseInt(req.query.offset) || 0;
    findAll([limit,offset])
        .then(function (results) {
            res.status(200).json(results);
        })
        .catch(function (err) {
            res.status(500).end();
        })
});

app.get("/api/book/:bookId", function (req,res) {
    findOne([req.params.bookId])
        .then(function (results) {
            res.status(200).json(results[0]);
        })
        .catch(function (err) {
            res.status(500).end();
        });
});

app.post("/api/book/save", function (req,res) {
    console.log(req.body.params);
    saveOne([req.body.params.title, req.body.params.author_firstname, req.body.params.author_lastname, req.body.params.id])
        .then(function (results) {
            res.status(200).json(results);
            console.log(results);
        })
        .catch(function (err) {
            res.status(500).end();
        });
});

app.get("/api/books/search", function (req, res) {
    console.log(req.query);
    var title = "%"+ req.query.title+"%";
    var name = "%"+ req.query.name +"%";
    if(req.query.title!=null && req.query.name!=null) {
        searchBooks([title, name, name])
            .then(function (results) {
                res.status(200).json(results);
                console.log(results);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).end();
            });
    }else if(req.query.title!=null){
        titleSearch([title])
            .then(function (results) {
                res.status(200).json(results);
                console.log(results);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).end();
            });
    }if(req.query.name!=null){
        nameSearch([name,name])
            .then(function (results) {
                res.status(200).json(results);
                console.log(results);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).end();
            });
    }
});

app.use(express.static(__dirname + "/public"));

app.listen(3000, function () {
    console.info("Store page start on port 3000")
});