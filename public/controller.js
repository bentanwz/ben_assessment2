(function () {
    angular
        .module("BookApp")
        .controller("listCtrl", listCtrl)
        .controller("editCtrl", editCtrl);
    
    
    
    function listCtrl(bookSvc, $state) {
        var vm = this;

        vm.books = "";
        
        vm.search = function (title, name) {
            bookSvc.search(title,name)
                .then(function (books) {
                    vm.books = books;
                })
                .catch(function (err) {
                    console.info("Book could not be found", err);
                });
        };
        
        vm.getBook = function (id) {
            $state.go("edit", {'bookId': id});
        };
        
        bookSvc.list()
            .then(function (books) {
                vm.books = books;
            })
            .catch(function (err) {
                console.info("An error has occured", err);
        });

    }
    
    listCtrl.$inject = ["bookSvc", "$state"];
    
    
    function editCtrl(bookSvc, $stateParams, $state) {
        var vm = this;
        vm.book = {};
        
        vm.cancel = function () {
            $state.go("list");
        };
        
        bookSvc.edit($stateParams.bookId)
            .then(function (book) {
                vm.book = book;
            })
            .catch(function (err) {
                console.info("An error occured", err);
            });
        
        vm.save = function () {
            bookSvc.save(vm.book)
                .then(function (result) {
                    console.info("Book has been saved");
                })
                .catch(function (err) {
                    console.info("An error occured", err);
            });
        }
    }
    editCtrl.$inject = ["bookSvc","$stateParams", "$state"];
       
})();