(function () {
    angular.module("BookApp")
        .config(BooksConfig);

    function BooksConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state("list", {
                url: "/list",
                templateUrl: "/list.html",
                controller: "listCtrl as ctrl"
            })
            .state("edit", {
                url: "/edit/:bookId",
                templateUrl: "/edit.html",
                controller: "editCtrl as ctrl"
            });

        $urlRouterProvider.otherwise("/list");
    }

    BooksConfig.$inject = ["$stateProvider", "$urlRouterProvider"];
})();